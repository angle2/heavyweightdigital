$(document).ready(function () {
  
  $('body')
  .on('mouseenter mouseleave','.dropdown-hover',toggleDropdown)
  .on('click', '.dropdown-menu a', toggleDropdown);

  $('select[data-placeholder]').change(function(e){
    console.log()
    if($.trim($(this).val()).length){
      $(this).addClass('selected');
    }else{
      $(this).removeClass('selected');
    }
    $(this).blur();
  });

  // $('.settings__list li a').click(function(e){
  //   console.log(e);
  //   e.preventDefault();
  // });

  $('.header-menu-button').click(function(e){
    $('header').toggleClass('menu');
  });

  // Datepicker
  $('#datepicker').daterangepicker({
    locale: {
      format: "MMMM, DD, YYYY",
      separator: ' '+String.fromCodePoint(0xe84b)+' ',
    },
    ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    "alwaysShowCalendars": true,
    "startDate": "11/07/2019",
    "endDate": "11/13/2019",
    "opens": "left"
}, function(start, end, label) {
  console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
});

  // if($('#datepicker').length){
  //   $('#datepicker').datepicker({
  //     dateFormat: 'MM d yyyy',
  //     range: true,
  //     multipleDatesSeparator: ' '+String.fromCodePoint(0xe84b)+' ',
  //     language: "en"
  //   });    
  // }

  // Tablesorter
  $('.table .sorting, .table .sorting_asc, .table .sorting_desc').click(function(e){    
    if($(this).hasClass('sorting_asc')){
      $(this).toggleClass('sorting_asc sorting_desc');
    }else if($(this).hasClass('sorting_desc')){
      $(this).toggleClass('sorting_desc sorting_asc');
    }else{
      $(this).closest('tr').find('.sorting_asc').toggleClass('sorting_asc sorting');
      $(this).closest('tr').find('.sorting_desc').toggleClass('sorting_desc sorting');
      $(this).toggleClass('sorting sorting_asc')      
    }
  });
  // Tooltips
  $('[data-toggle="tooltip"]').tooltip();

  // Charts
  if($('#dashboadr-chart').length){
    var dashboadrChart = Highcharts.chart('dashboadr-chart', {    
      chart: {
        marginRight: 150
      },
      xAxis: [{
          categories: ['Mon, 05th', 'Tue, 06th', 'Wed, 07th', 'AThu, 08thpr', 'Fri, 09th', 'Sat, 10th', 'Sun, 11th', 'Mon, 12th'],
          crosshair: true
      }],
      yAxis: [
        {
          title: ''
        }
      ],
      title: {
        text: 'Statistics'      
      },    
      plotOptions: {
        column: {
            stacking: 'normal'
        }
      },
      legend: {
        align: 'right',
        verticalAlign: 'top',
        layout: 'vertical',
        x: 0,
        y: 100,
        paddingTop: 0,
        itemMarginTop: 5,
        itemMarginLeft: 30,
        itemMarginBottom: 5,
        itemStyle: {
            lineHeight: '14px'
        }
      },
      series: [{
        name: 'Fraud Click',
        type: 'column',
        data: [20, 30, 70, 50, 15, 33, 20, 25],
        zIndex: 1,
        stack: 'male',
        color: '#FF808B',
        legendIndex: 2,      
        borderWidth: 0,
        //borderRadius: 10      
    }, {
        name: 'Paid Click',
        type: 'column',
        zIndex: 1,
        //borderRadius: 10,
        data: [20, 220, 77, 269, 163, 223, 141, 141],
        stack: 'male',
        color: '#199DBF',
        legendIndex: 1,
        borderWidth: 0
    }
    , {
      name: 'Fraud Ratio',
      type: 'areaspline',
      data: [10, 70, 77, 100, 150, 130, 90, 70],
      zIndex: 0,
      lineWidth: 0,
      legendIndex: 3,
      fillColor: {
        linearGradient: [0, 0, 0, 300],
        stops: [
            [0, '#199DBF'],
            [1, Highcharts.Color('#199DBF').setOpacity(0).get('rgba')]
        ]
    }
  }]    
  });
  }
  
  if($('#ratio-chart').length){
    var ratioChart = Highcharts.chart('ratio-chart', {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
        height: 170
    },
    title: {
        text: 'Clicks Ratio',
        align: 'left',
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    legend: {
      align: 'right',
      verticalAlign: 'middle',
      layout: 'vertical',
      x: -75,
      y: 0,
      paddingTop: 0,
      itemMarginTop: 5,
      itemMarginRight: 50,
      itemMarginLeft: -50,
      itemMarginBottom: 5,
      itemStyle: {
          lineHeight: '14px'
      }
    },
    plotOptions: {
      pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
              enabled: false
          },
          showInLegend: true,
          startAngle: -90,
          endAngle: 90,
          center: ['40%', '100%'],
          size: '240%'
      }    
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        innerSize: '70%',
        data: [
            {
              name: 'Total Click',
              y: 50.9,
              color: '#199DBF'
            },
            {
              name: 'Paid Click',
              y: 13.29,
              color: '#868DB8'
            },
            {
              name: 'Organic Click',
              y: 25.9,
              color: '#39CABE'
            },
            {
              name: 'Fraud Click',
              y: 9.91,
              color: '#FF808B'
            }        
        ]
    }]
    });
  }

  if($('#saving-chart').length){
    var dashboadrChart = Highcharts.chart('saving-chart', {    
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'column',
      },
      xAxis: [{
          categories: ['Mon, 05th', 'Tue, 06th', 'Wed, 07th', 'AThu, 08thpr', 'Fri, 09th', 'Sat, 10th', 'Sun, 11th', 'Mon, 12th'],
          crosshair: true
      }],
      yAxis: [
        {
          visible: false
        }
      ],
      title: {
        text: 'Potential Savings',
        align: 'left'        
      },    
      plotOptions: {
        
      },
      legend: {
        enabled: false
      },
      series: [{
        name: 'Save clicks',
        data: [20, 30, 70, 50, 15, 33, 20, 25],                
        color: '#199DBF',
        borderWidth: 0,
        //borderRadius: 10      
    }]    
  });
  }


});


function toggleDropdown (e) {
  const _d = $(e.target).closest('.dropdown'),
    _m = $('.dropdown-menu', _d);
  setTimeout(function(){
    const shouldOpen = e.type !== 'click' && _d.is(':hover');
    _m.toggleClass('show', shouldOpen);
    _d.toggleClass('show', shouldOpen);
    $('[data-toggle="dropdown"]', _d).attr('aria-expanded', shouldOpen);
  }, e.type === 'mouseleave' ? 100 : 0);
}